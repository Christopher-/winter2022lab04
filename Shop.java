import java.util.Scanner;
public class Shop {
	public static void main (String [] args) {
		Scanner sc = new Scanner(System.in);
		NvidiaCards[] forProductsArray = new NvidiaCards[3];
		for (int count = 0; count < forProductsArray.length; count++) {
			System.out.print("Enter the model of the gpu from Nvidia: ");
			String name = sc.nextLine();
			System.out.print("Enter the years of support for the gpu: ");
			int yearsOfSupport = Integer.parseInt(sc.nextLine());
			System.out.print("Enter the cost of the Nvidia card: ");
			double cost = Double.parseDouble(sc.nextLine());
			forProductsArray[count] = new NvidiaCards(name,yearsOfSupport,cost);
		}
		System.out.println("Before updates with setter methods:");
		System.out.println("The neme of the gpu model is: " + forProductsArray[forProductsArray.length-1].getName());
		System.out.println("The years of support of the gpu is: " + forProductsArray[forProductsArray.length-1].getYearsOfSupport() + " years");
		System.out.println("The cost of the last nvidia card entered is: " + forProductsArray[forProductsArray.length-1].getCost() + "$");
		System.out.print("Please enter a model to replace the last entered model: ");
		forProductsArray[forProductsArray.length-1].setName(sc.nextLine());
		System.out.print("Please enter years of support to replace the last entered values: ");
		forProductsArray[forProductsArray.length-1].setYearsOfSupport(Integer.parseInt(sc.nextLine()));
		System.out.print("Please enter the cost of the model to replace the last  one entered: ");
		forProductsArray[forProductsArray.length-1].setCost(Double.parseDouble(sc.nextLine()));
		System.out.println("After updates with setter methods:");
		System.out.println("The neme of the gpu model is: " + forProductsArray[forProductsArray.length-1].getName());
		System.out.println("The years of support of the gpu is: " + forProductsArray[forProductsArray.length-1].getYearsOfSupport() + " years");
		System.out.println("The cost of the last nvidia card entered is: " + forProductsArray[forProductsArray.length-1].getCost() + "$");
		forProductsArray[forProductsArray.length-1].demand();
	}
}
	