public class NvidiaCards {
	private double cost;
	private String name;
	private int yearsOfSupport;
	public void demand() {
	System.out.println("The years of support for " + name + " is " + yearsOfSupport + " years");
	if (this.cost > 200) {
		System.out.println("The cost of this graphics card will increase by: " + (this.cost*0.15) + "$ due to the current climate");
	}
	}
	
	//Mutator method for cost
	public void setCost(double newCost) {
		this.cost = newCost;
	}
	
	//Accessor method for cost
	public double getCost() {
		return this.cost;
	}
	//Mutator method for name
	public void setName(String newName) {
		this.name = newName;
	}
	//Accessor method for name
	public String getName() {
		return this.name;
	}
	//Mutator method for years of support
	public void setYearsOfSupport(int newYearsOfSupport) {
		this.yearsOfSupport = newYearsOfSupport;
	}
	//Accessor method for years of support
	public double getYearsOfSupport() {
		return this.yearsOfSupport;
	}
	//Constructor for NvidiaCards
	public NvidiaCards (String name, int yearsOfSupport, double cost) {
		this.name = name;
		this.yearsOfSupport = yearsOfSupport;
		this.cost = cost;
	}
}